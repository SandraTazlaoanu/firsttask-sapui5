sap.ui.define([
	"sap/ui/core/mvc/Controller"

], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.cerner.interns.SAPUI5_Demo.controller.View1", {
		onInit: function () {
		},
		onPressUser: function (oEvent) {
			var oItem = oEvent.getSource();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("detail", {

				userPath: encodeURIComponent(oItem.getBindingContext("user").getPath().substr(1))
				
				});
		},
		addUser: function () {
			var oUser = {
				userId: this.getView().byId("userIdInput").getValue(),
				username: this.getView().byId("usernameInput").getValue()
			}
			this.getView().getModel("user").getData().Users.push(oUser);
			this.getView().getModel("user").updateBindings();
		}
	});
});

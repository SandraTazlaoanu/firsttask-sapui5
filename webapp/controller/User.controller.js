sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/core/UIComponent",
	'sap/m/Button',
	'sap/m/Dialog',
	'sap/m/List',
	'sap/m/ObjectListItem'


], function (Controller, History, UIComponent, JSONModel, Button, Dialog, List, ObjectListItem) {
	"use strict";

	return Controller.extend("com.cerner.interns.SAPUI5_Demo.controller.User", {
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {

			this.getView().bindElement({
			
			path: decodeURIComponent("/" + oEvent.getParameter("arguments").userPath),
			
			model: "user"
			
			});
			
			},
		onPressBack: function () {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("overview", {}, true);
			}
		},
		openUserDetails: function () {
			if (!this.pressDialog) {
				this.pressDialog = new Dialog({
					title: 'Available Products',
					content: new List({
						items: {
							path: '/ProductCollection',
							template: new ObjectListItem({
								title: "{userId}",
								counter: "{username}"
							})
						}
					}),
					beginButton: new Button({
						type: ButtonType.Emphasized,
						text: 'OK',
						press: function () {
							this.pressDialog.close();
						}.bind(this)
					}),
					endButton: new Button({
						text: 'Close',
						press: function () {
							this.pressDialog.close();
						}.bind(this)
					})
				});

				//to get access to the global model
				this.getView().addDependent(this.pressDialog);
			}

			this.pressDialog.open();
		}
	});
});
